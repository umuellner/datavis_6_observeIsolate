
# This is the server logic for a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com
#

library(shiny)

shinyServer(function(input, output, session) {
  
  output$distPlot <- renderPlot({
    
    input$updateGraph
    isolate({
      
      x    <- faithful[, 2]
      
      bins <- seq(min(x), max(x), length.out = averageBins() + 1)
      
      
      hist(x, breaks = bins, col = 'darkgray', border = 'white')
    })
    
  })
  
  output$sliders <- renderUI({
    req(input$numInputs)
    inputs <- lapply(1:input$numInputs, function(x){
      inp <- sliderInput(paste0("bins", x),
                         paste0("Number of bins ", x, ":"),
                         min = 1,
                         max = 50,
                         value = 30)
      
      
      observe({
        max <- input$maxBins
        updateSliderInput(session, paste0("bins", x), max = max)
      })
      return(inp)
    });
    return(inputs)
  })
  
  
  averageBins <- reactive({
    req(input[[paste0("bins", input$numInputs)]])
    values <- sapply(1:input$numInputs, function(x){
      input[[paste0("bins", x)]]
    })
    as.integer(mean(values))
  })
  
})
